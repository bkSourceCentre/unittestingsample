﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExploringMSTest
{
    public class DanceFloor
    {
        public bool HasTwoLegs = false ;
        public bool JustCantDance = true;
        
        public bool CanDudeDance()
        {
            return HasTwoLegs;
        }

        public void TeachTheDudeHowToDance()
        {
            JustCantDance = false;
        }

        public string MakeTheDudeDance()
        {
            if(HasTwoLegs && !JustCantDance)
            {
                return "Yupp the dudes Dancing..!";
            }
            else
            {
                return "Naaah the dudes not Dancing...!";
            }
        }

        public string MakeDudeDanceAnyway()
        {
            HasTwoLegs = true;
            JustCantDance = false;
            return "Yuppp Got the dude dancing..!";
        }
    }
}
