﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExploringMSTest
{
    class Program
    {
        static void Main(string[] args)
        {

            DanceFloor aGeek = new DanceFloor();

            if (aGeek.CanDudeDance())
            {
                Console.WriteLine(aGeek.MakeTheDudeDance());
            }else
            {
                Console.WriteLine(aGeek.MakeDudeDanceAnyway());
            }

            DanceFloor anotherGeek = new DanceFloor();
            anotherGeek.JustCantDance = false;
            anotherGeek.HasTwoLegs = true;
            if (aGeek.CanDudeDance())
            {
                Console.WriteLine("Another dude... "+anotherGeek.MakeTheDudeDance());
            }
            else
            {
                Console.WriteLine("Another dude... " + anotherGeek.MakeDudeDanceAnyway());
            }

            Console.ReadKey();
        }
    }
}
